package com.tdk.mybatisplus.demo.common.entity;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;


/**
 * WebSocket消息类
 *
 * @author: taodingkai
 * @modified:
 * @version: 2022/08/08 11:45
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Builder
@ApiModel(value = "WebSocketMessage对象", description = "WebSocket消息类")

public class WebSocketMessage<T> {

    @NonNull
    @ApiModelProperty(value = "消息类型(REPLY 回复/ACTIVELY_SEND 主动发送)")
    private WebSocketResultTypeEnum type;
    
    @NonNull
    @ApiModelProperty(value = "主题")
    private String topic;
    
    @NonNull
    @ApiModelProperty(value = "标签")
    private String tag;

    @ApiModelProperty(value = "描述")
    private String description;
    
    @ApiModelProperty(value = "内容")
    private T data;
    
}