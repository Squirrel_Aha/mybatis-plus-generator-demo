package com.tdk.mybatisplus.demo.common.config;

import cn.hutool.core.util.StrUtil;
import io.minio.MinioClient;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Minio 配置类
 */
@Data
@Configuration
@ConfigurationProperties(prefix = "minio")
public class MinioConfig {

  private String  ip;
  private Integer port;
  private String  accessKey;
  private String  secretKey;
  private String  bucketName;
  /**
   * true:https  false:http
   */
  private boolean secure;

  public String getUrl() {
    if (secure) {
      return StrUtil.format("https://{}:{}", this.ip, this.port);
    } else {
      return StrUtil.format("http://{}:{}", this.ip, this.port);
    }
  }

  @Bean
  public MinioClient minioClient() {
    return MinioClient.builder()
        .credentials(accessKey, secretKey)
        .endpoint(ip, port, secure)
        .build();
  }
}
