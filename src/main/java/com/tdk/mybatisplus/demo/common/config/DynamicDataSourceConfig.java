package com.tdk.mybatisplus.demo.common.config;

import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.Map;

/**
 * <p>
 * 动态数据源配置
 * </p>
 *
 * @author: taodingkai
 * @modified:
 * @since: 2022/8/9 19:08
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "DynamicDataSourceConfig对象", description = "动态数据源配置")
@Component
@ConfigurationProperties(prefix = "spring.datasource.dynamic")
public class DynamicDataSourceConfig implements Serializable {
    private static final long serialVersionUID = 1L;
    private Map<String, Map<String, String>> datasource;
}