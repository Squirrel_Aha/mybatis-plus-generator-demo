package com.tdk.mybatisplus.demo.common.advice;

import com.alibaba.ttl.TransmittableThreadLocal;
import com.tdk.mybatisplus.demo.common.entity.HeaderTypeEnum;
import feign.RequestInterceptor;
import feign.RequestTemplate;
import org.springframework.stereotype.Component;

/**
 * <p>
 * feign拦截器
 * </p>
 *
 * @author: taodingkai
 * @modified:
 * @since: 2021/7/9 15:00
 */
@Component
public class DefaultFeignRequestInterceptor implements RequestInterceptor {

  public static ThreadLocal<HeaderTypeEnum> CURRENT_HEADER_TYPE = new TransmittableThreadLocal();
  
  @Override
  public void apply(RequestTemplate requestTemplate) {
    //读取ThreadLocal CURRENT_HEADER_TYPE 根绝类型设置请求头
  }
}
