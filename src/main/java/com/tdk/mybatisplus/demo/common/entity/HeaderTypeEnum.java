package com.tdk.mybatisplus.demo.common.entity;

/**
 * <p>
 * 请求头类型
 * </p>
 *
 * @author: taodingkai
 * @modified:
 * @since: 2021/7/13 19:17
 */
public enum HeaderTypeEnum {
  /**
   * 默认请求头
   */
  DEFAULT_HEADER,
  ;
}
