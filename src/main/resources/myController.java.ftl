package ${package.Controller};

import ${package.Service}.${table.serviceName};
import ${package.Entity}.${entity};
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.tdk.mybatisplus.demo.common.entity.Query;
import com.tdk.mybatisplus.demo.common.entity.Result;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;
<#if restControllerStyle>
import org.springframework.web.bind.annotation.RestController;
<#else>
import org.springframework.stereotype.Controller;
</#if>
<#if superControllerClassPackage??>
import ${superControllerClassPackage};
</#if>

/**
 * <p>
 * ${table.comment!} 前端控制器
 * </p>
 *
 * @author ${author}
 * @since ${date}
 */

@Slf4j
@Api(tags = "${table.comment!}")
<#if restControllerStyle>
@RestController
<#else>
@Controller
</#if>
@RequestMapping("<#if package.ModuleName??>/${package.ModuleName}</#if>/<#if controllerMappingHyphenStyle??>${controllerMappingHyphen}<#else>${table.entityPath}</#if>")
<#if kotlin>
public class ${table.controllerName}<#if superControllerClass??> : ${superControllerClass}()</#if>
<#else>
    <#if superControllerClass??>
public class ${table.controllerName} extends ${superControllerClass} {
    <#else>
public class ${table.controllerName} {

    @Autowired
    public ${table.serviceName} ${table.entityPath}Service;

    @ApiOperation(value = "${table.comment!}新增")
    @PostMapping("/save")
    public Result<<#list table.fields as field><#if field.keyFlag><#if field.keyIdentityFlag>${field.propertyType}<#elseif idType??>${field.propertyType}<#elseif field.convert>${field.propertyType}</#if></#if></#list>> save(@RequestBody ${entity} ${table.entityPath}){
        ${table.entityPath}Service.save(${table.entityPath});
        return Result.success(${table.entityPath}.get<#list table.fields as field><#if field.keyFlag><#if field.keyIdentityFlag>${field.capitalName}<#elseif idType??>${field.capitalName}<#elseif field.convert>${field.capitalName}</#if></#if></#list>());
    }

    @ApiOperation(value = "${table.comment!}按id删除")
    @PostMapping("/delete/{id}")
    public Result<<#list table.fields as field><#if field.keyFlag><#if field.keyIdentityFlag>${field.propertyType}<#elseif idType??>${field.propertyType}<#elseif field.convert>${field.propertyType}</#if></#if></#list>> delete(@PathVariable("id") <#list table.fields as field><#if field.keyFlag><#if field.keyIdentityFlag>${field.propertyType}<#elseif idType??>${field.propertyType}<#elseif field.convert>${field.propertyType}</#if></#if></#list> id){
        ${table.entityPath}Service.removeById(id);
        return Result.success(id);
    }
    
    @ApiOperation(value = "${table.comment!}按id修改")
    @PostMapping("/update/{id}")
    public Result<<#list table.fields as field><#if field.keyFlag><#if field.keyIdentityFlag>${field.propertyType}<#elseif idType??>${field.propertyType}<#elseif field.convert>${field.propertyType}</#if></#if></#list>> update(@PathVariable("id") <#list table.fields as field><#if field.keyFlag><#if field.keyIdentityFlag>${field.propertyType}<#elseif idType??>${field.propertyType}<#elseif field.convert>${field.propertyType}</#if></#if></#list> id, @RequestBody ${entity} ${table.entityPath}){
        ${table.entityPath}.set<#list table.fields as field><#if field.keyFlag><#if field.keyIdentityFlag>${field.capitalName}<#elseif idType??>${field.capitalName}<#elseif field.convert>${field.capitalName}</#if></#if></#list>(id);
        ${table.entityPath}Service.updateById(${table.entityPath});
        return Result.success(${table.entityPath}.get<#list table.fields as field><#if field.keyFlag><#if field.keyIdentityFlag>${field.capitalName}<#elseif idType??>${field.capitalName}<#elseif field.convert>${field.capitalName}</#if></#if></#list>());
    }

    @ApiOperation(value = "${table.comment!}按id查询")
    @GetMapping("/get/{id}")
    public Result<${entity}> get(@PathVariable("id") <#list table.fields as field><#if field.keyFlag><#if field.keyIdentityFlag>${field.propertyType}<#elseif idType??>${field.propertyType}<#elseif field.convert>${field.propertyType}</#if></#if></#list> id){
        ${entity} ${table.entityPath} = ${table.entityPath}Service.getById(id);
        return Result.success(${table.entityPath});
    }
 
    @ApiOperation(value = "${table.comment!}条件查询带分页")
    @PostMapping("/page")
    public Result<IPage<${entity}>> page(@RequestBody Query<${entity}> ${table.entityPath}Query){
        Long pageNum=${table.entityPath}Query.getPageNum();
        Long pageSize=${table.entityPath}Query.getPageSize();
        ${entity} ${table.entityPath}=${table.entityPath}Query.getCondition();
        IPage<${entity}> ${table.entityPath}List = ${table.entityPath}Service.page(new Page<>(pageNum, pageSize), new QueryWrapper<>(${table.entityPath}));
        return Result.success(${table.entityPath}List);
    }

    @ApiOperation(value = "${table.comment!}条件查询")
    @PostMapping("/list")
        public Result<List<${entity}>> list(@RequestBody ${entity} ${table.entityPath}){
        List<${entity}> ${table.entityPath}List = ${table.entityPath}Service.list(new QueryWrapper<>(${table.entityPath}));
        return Result.success(${table.entityPath}List);
    }
    </#if>

}
</#if>