package ${package.Service};

import ${package.Entity}.${entity};
import ${superServiceClassPackage};
import java.util.Collection;

/**
 * <p>
 * ${table.comment!} 服务类
 * </p>
 *
 * @author ${author}
 * @since ${date}
 */
<#if kotlin>
interface ${table.serviceName} : ${superServiceClass}<${entity}>
<#else>
public interface ${table.serviceName} extends ${superServiceClass}<${entity}> {

    /**
     * <p>
     * 批量insert,主键冲突时update
     * </p>
     *
     *
     * @param list
     * @author: taodingkai
     * @since: 2021/10/11 20:59
     */
    void batchInsertOnDuplicateKeyUpdate(Collection<${entity}> list);
    
    /**
     * <p>
     * 批量insert,主键冲突时忽略
     * </p>
     *
     *
     * @param list
     * @author: taodingkai
     * @since: 2021/11/3 17:09
     */
    void batchInsertOnDuplicateKeyIgnore(Collection<${entity}> list);

}
</#if>
