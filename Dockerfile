FROM java:8-alpine
RUN sed -i 's/dl-cdn.alpinelinux.org/mirrors.aliyun.com/g' /etc/apk/repositories
RUN apk add --no-cache tzdata \
    && ln -snf /usr/share/zoneinfo/Asia/Shanghai /etc/localtime \
    && echo "Asia/Shanghai" > /etc/timezone
RUN apk add --update ttf-dejavu fontconfig
ARG JAR_FILE=target/app.jar
ARG CONFIG_FILE=target/classes/application.yml
ARG LOG_FILE=target/classes/logback.xml
COPY ${JAR_FILE} /app.jar
COPY ${CONFIG_FILE} /application.yml
COPY ${LOG_FILE} /logback.xml
EXPOSE 8080
#切换工作目录
WORKDIR /
#运行程序
CMD java -jar -Dfile.encoding=UTF-8 app.jar