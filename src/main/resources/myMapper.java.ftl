package ${package.Mapper};

import ${package.Entity}.${entity};
import ${superMapperClassPackage};
import java.util.Collection;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * ${table.comment!} Mapper 接口
 * </p>
 *
 * @author ${author}
 * @since ${date}
 */
<#if kotlin>
interface ${table.mapperName} : ${superMapperClass}<${entity}>
<#else>
public interface ${table.mapperName} extends ${superMapperClass}<${entity}> {

   /**
   * <p>
   * 批量insert,主键冲突时update
   * </p>
   *
   *
   * @param list
   * @author: taodingkai
   * @since: 2021/10/11 20:59
   */
   void batchInsertOnDuplicateKeyUpdate(@Param("list") Collection<${entity}> list);
   
   /**
   * <p>
   * 批量insert,主键冲突时忽略
   * </p>
   *
   *
   * @param list
   * @author: taodingkai
   * @since: 2021/11/3 17:09
   */
   void batchInsertOnDuplicateKeyIgnore(@Param("list") Collection<${entity}> list);

}
</#if>
