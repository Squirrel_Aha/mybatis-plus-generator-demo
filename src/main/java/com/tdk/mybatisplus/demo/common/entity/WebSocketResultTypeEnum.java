package com.tdk.mybatisplus.demo.common.entity;


/**
 * WebSocketResultType
 *
 * @author: taodingkai
 * @modified:
 * @version: 2022/08/08 15:23
 */
public enum WebSocketResultTypeEnum  implements ICodeNameMapping<String,String> {
    /**
     * 回复
     */
    REPLY("REPLY","回复"),
    /**
     * 主动发送
     */
    ACTIVELY_SEND("ACTIVELY_SEND","主动发送"),
    ;


    /**
     * 编码
     */
    private String code;
    /**
     * 名称
     */
    private String name;

    WebSocketResultTypeEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    @Override
    public String getCode() {
        return code;
    }

    @Override
    public String getName() {
        return name;
    }
}
