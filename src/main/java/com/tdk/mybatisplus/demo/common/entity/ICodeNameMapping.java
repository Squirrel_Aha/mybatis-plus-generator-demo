package com.tdk.mybatisplus.demo.common.entity;

/**
 * <p>
 * code name mapping 枚举接口类
 * </p>
 *
 * @author: taodingkai
 * @modified:
 * @since: 2022/08/08 12:57
 */
public interface ICodeNameMapping<K,V> {
  /**
   * <p>
   * 获取编码
   * </p>
   *
   * @return java.lang.Integer
   * @author: taodingkai
   * @since: 2022/5/14 13:56
   */
  K getCode();

  /**
   * <p>
   * 获取名称
   * </p>
   *
   * @return java.lang.String
   * @author: taodingkai
   * @since: 2022/5/14 13:56
   */
  V getName();
}