package com.tdk.mybatisplus.demo.common.entity;

import javax.validation.constraints.NotNull;

/**
 * <p>
 * 两个参数的函数
 * </p>
 *
 * @author: taodingkai
 * @modified:
 * @since: 2022/8/23 11:29 
 */
public interface Function2Param<P1, P2, R> {
    /**
     * <p>
     * 两个参数的函数
     * </p>
     *
     * @param p1
     * @param p2
     * @return R
     * @author: taodingkai
     * @since: 2022/8/18 19:14
     */
    R apply(@NotNull P1 p1, @NotNull P2 p2);
}