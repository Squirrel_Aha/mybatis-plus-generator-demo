package com.tdk.mybatisplus.demo;

import com.tdk.mybatisplus.demo.api.HelloApi;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 测试接口
 * </p>
 *
 * @author: taodingkai
 * @modified:
 * @since: 2021/6/2 14:13
 */
@RestController
public class HelloController implements HelloApi {

  @Override
  @RequestMapping("/hello")
  public String hello(@RequestParam("hello") String hello) {
    return "\r\n"
        + "-------------------------------\r\n"
        + "| Greetings from Spring Boot! |\r\n"
        + "-------------------------------\r\n";
  }
}