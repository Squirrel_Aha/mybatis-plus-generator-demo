package com.tdk.mybatisplus.demo.common.entity;


import io.swagger.annotations.ApiModelProperty;
import lombok.NonNull;
import org.springframework.beans.BeanUtils;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static com.tdk.mybatisplus.demo.common.entity.WebSocketResultTypeEnum.ACTIVELY_SEND;
import static com.tdk.mybatisplus.demo.common.entity.WebSocketResultTypeEnum.REPLY;

/**
 * WebSocketResult枚举
 *
 * @author: taodingkai
 * @modified:
 * @version: 2022/08/08 15:23
 */
public enum WebSocketResultEnum {
    /**
     * 回复
     */
    REPLY_MESSAGE(REPLY, "reply", "*", "回复收到的消息", null),
    NEW_ALARM(ACTIVELY_SEND, "alarm", "new", "有新的告警",null),
    NEW_MONITORING_DATA(ACTIVELY_SEND, "monitoring_data", "new", "新的监测数据",null),

    ;

    @NonNull
    @ApiModelProperty(value = "消息类型(REPLY 回复/ACTIVELY_SEND 主动发送)")
    private WebSocketResultTypeEnum type;

    @NonNull
    @ApiModelProperty(value = "主题")
    private String topic;

    @NonNull
    @ApiModelProperty(value = "标签")
    private String tag;

    @ApiModelProperty(value = "描述")
    private String description;

    @ApiModelProperty(value = "内容")
    private Object data;

    public WebSocketResultTypeEnum getType() {
        return type;
    }

    public void setType(WebSocketResultTypeEnum type) {
        this.type = type;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    WebSocketResultEnum(@NonNull WebSocketResultTypeEnum type, @NonNull String topic, @NonNull String tag, String description, Object data) {
        this.type = type;
        this.topic = topic;
        this.tag = tag;
        this.description = description;
        this.data = data;
    }

    public WebSocketMessage<Object> toWebSocketMessage() {
        WebSocketMessage<Object> webSocketMessage = new WebSocketMessage<Object>();
        BeanUtils.copyProperties(this, webSocketMessage);
        return webSocketMessage;
    }

    public WebSocketMessage<Object> toWebSocketMessage(String description, Object data) {
        WebSocketMessage webSocketMessage = this.toWebSocketMessage();
        webSocketMessage.setDescription(description);
        webSocketMessage.setData(data);
        return webSocketMessage;
    }

    public WebSocketMessage<Object> toWebSocketMessage(Object data) {
        WebSocketMessage webSocketMessage = this.toWebSocketMessage();
        webSocketMessage.setData(data);
        return webSocketMessage;
    }

    public static List<WebSocketMessage<Object>> toWebSocketMessageList() {
        return Arrays.asList(WebSocketResultEnum.values()).stream().map(WebSocketResultEnum::toWebSocketMessage).collect(Collectors.toList());
    }
}