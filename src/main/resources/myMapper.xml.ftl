<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<mapper namespace="${package.Mapper}.${table.mapperName}">

<#if enableCache>
    <!-- 开启二级缓存 -->
    <cache type="org.mybatis.caches.ehcache.LoggingEhcache"/>

</#if>
<#if baseResultMap>
    <!-- 通用查询映射结果 -->
    <resultMap id="BaseResultMap" type="${package.Entity}.${entity}">
<#list table.fields as field>
<#if field.keyFlag><#--生成主键排在第一位-->
        <id column="${field.name}" property="${field.propertyName}" />
</#if>
</#list>
<#list table.commonFields as field><#--生成公共字段 -->
        <result column="${field.name}" property="${field.propertyName}" />
</#list>
<#list table.fields as field>
<#if !field.keyFlag><#--生成普通字段 -->
        <result column="${field.name}" property="${field.propertyName}" />
</#if>
</#list>
    </resultMap>

</#if>
    
<#if baseColumnList>
    <!-- 通用查询结果列 -->
    <sql id="Base_Column_List">
        <trim prefix=" " suffix=" " suffixOverrides=",">
            <#list table.fields as field><#if field.keyFlag><#--生成主键排在第一位-->`${field.name}`,</#if></#list><#list table.commonFields as field><#--生成公共字段 -->`${field.name}`, </#list><#list table.fields as field><#if !field.keyFlag><#--生成普通字段 --> `${field.name}`,</#if></#list>
        </trim>
    </sql>
</#if>
    
    <!-- 通用insert列表 -->
    <sql id="Base_Value_List">
        <trim prefix=" " suffix=" " suffixOverrides=",">
            <#list table.fields as field><#if field.keyFlag>${r"#{entity."+field.propertyName+"}"},</#if></#list><#list table.commonFields as field>${r"#{entity."+field.propertyName+"}"},</#list><#list table.fields as field><#if !field.keyFlag>${r"#{entity."+field.propertyName+"}"},</#if></#list>
        </trim>
    </sql>

    <sql id="On_Duplicate_Key_Update">
        <trim prefix=" " suffix=" " suffixOverrides=",">
    <#list table.fields as field>
        <#if field.keyFlag><#--生成主键排在第一位-->
            `${field.name}`=VALUES(`${field.name}`),
        </#if>
    </#list>
    <#list table.commonFields as field><#--生成公共字段 -->
            `${field.name}`=VALUES(`${field.name}`),
    </#list>
    <#list table.fields as field>
        <#if !field.keyFlag><#--生成普通字段 -->
            `${field.name}`=VALUES(`${field.name}`),
        </#if>
    </#list>
        </trim>
    </sql>
    
    <!--批量insert,主键冲突时update-->
    <insert id="batchInsertOnDuplicateKeyUpdate">
        INSERT INTO `${table.name}` (<include refid="Base_Column_List"/>) VALUES
        <foreach collection="list" item="entity" separator="," open="" close="">
            <trim prefix="(" suffix=")" suffixOverrides=","><include refid="Base_Value_List"/></trim>
        </foreach>
        ON DUPLICATE KEY UPDATE
        <include refid="On_Duplicate_Key_Update"/>
    </insert>

    <!--批量insert,主键冲突时忽略-->
    <insert id="batchInsertOnDuplicateKeyIgnore">
        INSERT IGNORE INTO `${table.name}` (<include refid="Base_Column_List"/>) VALUES
        <foreach collection="list" item="entity" separator="," open="" close="">
            <trim prefix="(" suffix=")" suffixOverrides=","><include refid="Base_Value_List"/></trim>
        </foreach>
    </insert>
    
</mapper>
