package com.tdk.mybatisplus.demo.common.exception;

import cn.hutool.core.util.StrUtil;

/**
 * <p>
 * minio工具类用的
 * </p>
 *
 * @author: taodingkai
 * @modified:
 * @since: 2022/5/19 13:44
 */
public class GlobalException extends RuntimeException {

  private String detailMessage;

  public GlobalException(String message, Object... params) {
    super();
    if (params.length > 0) {
      this.detailMessage = StrUtil.format(message, params);
    } else {
      this.detailMessage = message;
    }
  }

  @Override
  public String getMessage() {
    return detailMessage;
  }
}
