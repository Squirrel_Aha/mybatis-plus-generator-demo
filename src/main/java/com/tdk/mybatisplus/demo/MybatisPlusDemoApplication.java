package com.tdk.mybatisplus.demo;

import static com.gitee.sunchenbin.mybatis.actable.command.JavaToMysqlType.javaToMysqlTypeMap;

import com.gitee.sunchenbin.mybatis.actable.constants.MySqlTypeConstant;
import com.spring4all.swagger.EnableSwagger2Doc;
import java.util.TimeZone;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
@EnableSwagger2Doc
@EnableFeignClients
@EnableScheduling
@EnableAspectJAutoProxy
@MapperScan("com.gitee.sunchenbin.mybatis.actable.dao.*")
@ComponentScan(basePackages = {"com.tdk","com.gitee.sunchenbin.mybatis.actable.manager.*"})
public class MybatisPlusDemoApplication {


  public static void main(String[] args) {
    /**
     * 设置默认时区
     */
    System.setProperty("user.timezone", "Asia/Shanghai");
    TimeZone.setDefault(TimeZone.getTimeZone("Asia/Shanghai"));
    //ACTable自动建表
    //有些字段 varchar(255)长度不够
    //主键是String的加上@ColumnType(MySqlTypeConstant.VARCHAR)替换默认设置
    javaToMysqlTypeMap.put("class java.lang.String", MySqlTypeConstant.LONGTEXT);
    SpringApplication.run(MybatisPlusDemoApplication.class, args);
  }

}
