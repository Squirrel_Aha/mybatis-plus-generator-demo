package com.tdk.mybatisplus.demo.sys.service.impl;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.tdk.mybatisplus.demo.sys.entity.User;
import com.tdk.mybatisplus.demo.sys.dao.UserMapper;
import com.tdk.mybatisplus.demo.sys.service.UserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户表 服务实现类
 * </p>
 *
 * @author tdk
 * @since 2021-05-18
 */
@Service
@DS("test2")
//当动态数据源失效一般是aop失效,最简单的方式改成类似下面这样
// A类调用 B类 和 C类 的方法，A类本身不带@DS注解，B和C带@DS注解且各自数据源不同
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {

}
