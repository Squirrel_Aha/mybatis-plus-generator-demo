package com.tdk.mybatisplus.demo.runner;

import javax.annotation.Resource;
import javax.annotation.Resources;
import lombok.extern.slf4j.Slf4j;
import org.mapstruct.Qualifier;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(value = 301)
@Slf4j
public class DefaultCommandLineRunner implements CommandLineRunner {

  @Resource(name = "greetings")
  private String greetings;

  @Override
  public void run(String... strings) throws Exception {
    log.info(greetings);
  }
}
