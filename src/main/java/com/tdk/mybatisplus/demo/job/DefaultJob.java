package com.tdk.mybatisplus.demo.job;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@ConditionalOnProperty(name = "default.job.enable", havingValue = "true",matchIfMissing = true)
public class DefaultJob {

  @Scheduled(fixedDelayString = "60000", initialDelayString = "3000")
  public void printRunning() {
    log.info("Running...");
  }
}