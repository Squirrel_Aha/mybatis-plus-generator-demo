package com.tdk.mybatisplus.demo.common.entity;

import com.google.common.eventbus.EventBus;

import static com.tdk.mybatisplus.demo.common.entity.Constants.DEFAULT_EVENT_BUS;

/**
 * <p>
 * EventBus使用者
 * </p>
 *
 * @author: taodingkai
 * @modified:
 * @since: 2022/8/11 10:45
 */
public interface EventBusUser {
    /**
     * 注册
     */
    void register();
    
    /**
     * <p>
     * 获取默认的eventBus
     * </p>
     *
     *   
     * @author: taodingkai
     * @since: 2022/8/11 10:53 
     * @return
     */
    default EventBus getDefaultEventBus(){
        return DEFAULT_EVENT_BUS;
    }
}
