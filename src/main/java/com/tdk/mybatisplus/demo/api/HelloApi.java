package com.tdk.mybatisplus.demo.api;

import io.swagger.annotations.ApiOperation;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@FeignClient(name = "hello-api", url = "${hello.api.url:localhost:8080}", path = "/")
@Component
public interface HelloApi {

  @ApiOperation(value = "hello")
  @GetMapping(value = "/hello", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
  @ResponseBody
  String hello(@RequestParam("hello") String hello);
}