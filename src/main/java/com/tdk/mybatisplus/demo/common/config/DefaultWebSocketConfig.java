package com.tdk.mybatisplus.demo.common.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.config.annotation.WebSocketConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;

/**
 * <p>
 * swagger 配置
 * </p>
 *
 * @author: taodingkai
 * @modified:
 * @since: 2022/08/08 14:16
 */
@Configuration
@EnableWebSocket
public class DefaultWebSocketConfig implements WebSocketConfigurer {

    @Override
    public void registerWebSocketHandlers(WebSocketHandlerRegistry registry) {
        registry.addHandler(defaultWebSocketHandler(), "/websocket/spring").setAllowedOrigins("*");

    }

    @Bean
    public WebSocketHandler defaultWebSocketHandler() {
        return new DefaultWebSocketHandler();
    }
}
