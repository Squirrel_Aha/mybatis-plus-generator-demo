# API文档


<a name="overview"></a>
## 概览

### URI scheme
*域名* : localhost:8090  
*基础路径* : /


### 标签

* basic-error-controller : Basic Error Controller
* hello-controller : Hello Controller
* 用户表 : User Controller




<a name="paths"></a>
## 资源

<a name="basic-error-controller_resource"></a>
### Basic-error-controller
Basic Error Controller


<a name="errorhtmlusingpost"></a>
#### errorHtml
```
POST /error
```


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|[ModelAndView](#modelandview)|
|**201**|Created|无内容|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|


##### 消耗

* `application/json`


##### 生成

* `text/html`


##### 安全

|类型|名称|作用域|
|---|---|---|
|**apiKey**|**[Authorization](#authorization)**|global|


<a name="errorhtmlusingget"></a>
#### errorHtml
```
GET /error
```


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|[ModelAndView](#modelandview)|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|


##### 生成

* `text/html`


##### 安全

|类型|名称|作用域|
|---|---|---|
|**apiKey**|**[Authorization](#authorization)**|global|


<a name="errorhtmlusingput"></a>
#### errorHtml
```
PUT /error
```


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|[ModelAndView](#modelandview)|
|**201**|Created|无内容|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|


##### 消耗

* `application/json`


##### 生成

* `text/html`


##### 安全

|类型|名称|作用域|
|---|---|---|
|**apiKey**|**[Authorization](#authorization)**|global|


<a name="errorhtmlusingdelete"></a>
#### errorHtml
```
DELETE /error
```


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|[ModelAndView](#modelandview)|
|**204**|No Content|无内容|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|


##### 生成

* `text/html`


##### 安全

|类型|名称|作用域|
|---|---|---|
|**apiKey**|**[Authorization](#authorization)**|global|


<a name="errorhtmlusingpatch"></a>
#### errorHtml
```
PATCH /error
```


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|[ModelAndView](#modelandview)|
|**204**|No Content|无内容|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|


##### 消耗

* `application/json`


##### 生成

* `text/html`


##### 安全

|类型|名称|作用域|
|---|---|---|
|**apiKey**|**[Authorization](#authorization)**|global|


<a name="errorhtmlusinghead"></a>
#### errorHtml
```
HEAD /error
```


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|[ModelAndView](#modelandview)|
|**204**|No Content|无内容|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|


##### 消耗

* `application/json`


##### 生成

* `text/html`


##### 安全

|类型|名称|作用域|
|---|---|---|
|**apiKey**|**[Authorization](#authorization)**|global|


<a name="errorhtmlusingoptions"></a>
#### errorHtml
```
OPTIONS /error
```


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|[ModelAndView](#modelandview)|
|**204**|No Content|无内容|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|


##### 消耗

* `application/json`


##### 生成

* `text/html`


##### 安全

|类型|名称|作用域|
|---|---|---|
|**apiKey**|**[Authorization](#authorization)**|global|


<a name="hello-controller_resource"></a>
### Hello-controller
Hello Controller


<a name="hellousingpost"></a>
#### hello
```
POST /hello
```


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Query**|**hello**  <br>*必填*|hello|string|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|string|
|**201**|Created|无内容|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|


##### 消耗

* `application/json`


##### 生成

* `\*/*`


##### 安全

|类型|名称|作用域|
|---|---|---|
|**apiKey**|**[Authorization](#authorization)**|global|


<a name="hellousingget"></a>
#### hello
```
GET /hello
```


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Query**|**hello**  <br>*必填*|hello|string|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|string|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|


##### 生成

* `\*/*`


##### 安全

|类型|名称|作用域|
|---|---|---|
|**apiKey**|**[Authorization](#authorization)**|global|


<a name="hellousingput"></a>
#### hello
```
PUT /hello
```


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Query**|**hello**  <br>*必填*|hello|string|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|string|
|**201**|Created|无内容|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|


##### 消耗

* `application/json`


##### 生成

* `\*/*`


##### 安全

|类型|名称|作用域|
|---|---|---|
|**apiKey**|**[Authorization](#authorization)**|global|


<a name="hellousingdelete"></a>
#### hello
```
DELETE /hello
```


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Query**|**hello**  <br>*必填*|hello|string|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|string|
|**204**|No Content|无内容|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|


##### 生成

* `\*/*`


##### 安全

|类型|名称|作用域|
|---|---|---|
|**apiKey**|**[Authorization](#authorization)**|global|


<a name="hellousingpatch"></a>
#### hello
```
PATCH /hello
```


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Query**|**hello**  <br>*必填*|hello|string|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|string|
|**204**|No Content|无内容|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|


##### 消耗

* `application/json`


##### 生成

* `\*/*`


##### 安全

|类型|名称|作用域|
|---|---|---|
|**apiKey**|**[Authorization](#authorization)**|global|


<a name="hellousinghead"></a>
#### hello
```
HEAD /hello
```


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Query**|**hello**  <br>*必填*|hello|string|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|string|
|**204**|No Content|无内容|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|


##### 消耗

* `application/json`


##### 生成

* `\*/*`


##### 安全

|类型|名称|作用域|
|---|---|---|
|**apiKey**|**[Authorization](#authorization)**|global|


<a name="hellousingoptions"></a>
#### hello
```
OPTIONS /hello
```


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Query**|**hello**  <br>*必填*|hello|string|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|string|
|**204**|No Content|无内容|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|


##### 消耗

* `application/json`


##### 生成

* `\*/*`


##### 安全

|类型|名称|作用域|
|---|---|---|
|**apiKey**|**[Authorization](#authorization)**|global|


<a name="abc6bb045e2f92dbb375dfa5df84c7b9"></a>
### 用户表
User Controller


<a name="deleteusingpost"></a>
#### 用户表按id删除
```
POST /sys/user/delete/{id}
```


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Path**|**id**  <br>*必填*|id|integer (int64)|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|[Result对象«long»](#e650c22cf1d4cd5b03a45fe785e2c7ab)|
|**201**|Created|无内容|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|


##### 消耗

* `application/json`


##### 生成

* `\*/*`


##### 安全

|类型|名称|作用域|
|---|---|---|
|**apiKey**|**[Authorization](#authorization)**|global|


<a name="getusingget"></a>
#### 用户表按id查询
```
GET /sys/user/get/{id}
```


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Path**|**id**  <br>*必填*|id|integer (int64)|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|[Result对象«User对象»](#b3631857fd107948f003b33150259431)|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|


##### 生成

* `\*/*`


##### 安全

|类型|名称|作用域|
|---|---|---|
|**apiKey**|**[Authorization](#authorization)**|global|


<a name="listusingpost"></a>
#### 用户表条件查询带分页
```
POST /sys/user/list
```


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Body**|**userQuery**  <br>*必填*|userQuery|[Query对象«User对象»](#b3d919636eb055984be88c0eb424fa6e)|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|[Result对象«IPage«User对象»»](#21da4a646b2f8c51737eb32ff5886030)|
|**201**|Created|无内容|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|


##### 消耗

* `application/json`


##### 生成

* `\*/*`


##### 安全

|类型|名称|作用域|
|---|---|---|
|**apiKey**|**[Authorization](#authorization)**|global|


<a name="saveusingpost"></a>
#### 用户表新增
```
POST /sys/user/save
```


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Body**|**user**  <br>*必填*|user|[User对象](#95cde29d19638e8b05882b3550fba87d)|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|[Result对象«long»](#e650c22cf1d4cd5b03a45fe785e2c7ab)|
|**201**|Created|无内容|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|


##### 消耗

* `application/json`


##### 生成

* `\*/*`


##### 安全

|类型|名称|作用域|
|---|---|---|
|**apiKey**|**[Authorization](#authorization)**|global|


<a name="updateusingpost"></a>
#### 用户表按id修改
```
POST /sys/user/update/{id}
```


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Path**|**id**  <br>*必填*|id|integer (int64)|
|**Body**|**user**  <br>*必填*|user|[User对象](#95cde29d19638e8b05882b3550fba87d)|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|[Result对象«long»](#e650c22cf1d4cd5b03a45fe785e2c7ab)|
|**201**|Created|无内容|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|


##### 消耗

* `application/json`


##### 生成

* `\*/*`


##### 安全

|类型|名称|作用域|
|---|---|---|
|**apiKey**|**[Authorization](#authorization)**|global|




<a name="definitions"></a>
## 定义

<a name="adf7e4bb4e305e6802291bf0abd7021e"></a>
### IPage«User对象»

|名称|类型|
|---|---|
|**current**  <br>*可选*|integer (int64)|
|**hitCount**  <br>*可选*|boolean|
|**pages**  <br>*可选*|integer (int64)|
|**records**  <br>*可选*|< [User对象](#95cde29d19638e8b05882b3550fba87d) > array|
|**searchCount**  <br>*可选*|boolean|
|**size**  <br>*可选*|integer (int64)|
|**total**  <br>*可选*|integer (int64)|


<a name="modelandview"></a>
### ModelAndView

|名称|类型|
|---|---|
|**empty**  <br>*可选*|boolean|
|**model**  <br>*可选*|object|
|**modelMap**  <br>*可选*|< string, object > map|
|**reference**  <br>*可选*|boolean|
|**status**  <br>*可选*|enum (100 CONTINUE, 101 SWITCHING_PROTOCOLS, 102 PROCESSING, 103 CHECKPOINT, 200 OK, 201 CREATED, 202 ACCEPTED, 203 NON_AUTHORITATIVE_INFORMATION, 204 NO_CONTENT, 205 RESET_CONTENT, 206 PARTIAL_CONTENT, 207 MULTI_STATUS, 208 ALREADY_REPORTED, 226 IM_USED, 300 MULTIPLE_CHOICES, 301 MOVED_PERMANENTLY, 302 FOUND, 302 MOVED_TEMPORARILY, 303 SEE_OTHER, 304 NOT_MODIFIED, 305 USE_PROXY, 307 TEMPORARY_REDIRECT, 308 PERMANENT_REDIRECT, 400 BAD_REQUEST, 401 UNAUTHORIZED, 402 PAYMENT_REQUIRED, 403 FORBIDDEN, 404 NOT_FOUND, 405 METHOD_NOT_ALLOWED, 406 NOT_ACCEPTABLE, 407 PROXY_AUTHENTICATION_REQUIRED, 408 REQUEST_TIMEOUT, 409 CONFLICT, 410 GONE, 411 LENGTH_REQUIRED, 412 PRECONDITION_FAILED, 413 PAYLOAD_TOO_LARGE, 413 REQUEST_ENTITY_TOO_LARGE, 414 URI_TOO_LONG, 414 REQUEST_URI_TOO_LONG, 415 UNSUPPORTED_MEDIA_TYPE, 416 REQUESTED_RANGE_NOT_SATISFIABLE, 417 EXPECTATION_FAILED, 418 I_AM_A_TEAPOT, 419 INSUFFICIENT_SPACE_ON_RESOURCE, 420 METHOD_FAILURE, 421 DESTINATION_LOCKED, 422 UNPROCESSABLE_ENTITY, 423 LOCKED, 424 FAILED_DEPENDENCY, 426 UPGRADE_REQUIRED, 428 PRECONDITION_REQUIRED, 429 TOO_MANY_REQUESTS, 431 REQUEST_HEADER_FIELDS_TOO_LARGE, 451 UNAVAILABLE_FOR_LEGAL_REASONS, 500 INTERNAL_SERVER_ERROR, 501 NOT_IMPLEMENTED, 502 BAD_GATEWAY, 503 SERVICE_UNAVAILABLE, 504 GATEWAY_TIMEOUT, 505 HTTP_VERSION_NOT_SUPPORTED, 506 VARIANT_ALSO_NEGOTIATES, 507 INSUFFICIENT_STORAGE, 508 LOOP_DETECTED, 509 BANDWIDTH_LIMIT_EXCEEDED, 510 NOT_EXTENDED, 511 NETWORK_AUTHENTICATION_REQUIRED)|
|**view**  <br>*可选*|[View](#view)|
|**viewName**  <br>*可选*|string|


<a name="b3d919636eb055984be88c0eb424fa6e"></a>
### Query对象«User对象»
带分页的查询


|名称|说明|类型|
|---|---|---|
|**condition**  <br>*可选*|条件|[User对象](#95cde29d19638e8b05882b3550fba87d)|
|**pageNum**  <br>*可选*|页码|integer (int64)|
|**pageSize**  <br>*可选*|每页大小|integer (int64)|


<a name="21da4a646b2f8c51737eb32ff5886030"></a>
### Result对象«IPage«User对象»»
公共返回结果类


|名称|说明|类型|
|---|---|---|
|**code**  <br>*可选*|编码|integer (int32)|
|**data**  <br>*可选*|数据|[IPage«User对象»](#adf7e4bb4e305e6802291bf0abd7021e)|
|**message**  <br>*可选*|消息|string|


<a name="b3631857fd107948f003b33150259431"></a>
### Result对象«User对象»
公共返回结果类


|名称|说明|类型|
|---|---|---|
|**code**  <br>*可选*|编码|integer (int32)|
|**data**  <br>*可选*|数据|[User对象](#95cde29d19638e8b05882b3550fba87d)|
|**message**  <br>*可选*|消息|string|


<a name="e650c22cf1d4cd5b03a45fe785e2c7ab"></a>
### Result对象«long»
公共返回结果类


|名称|说明|类型|
|---|---|---|
|**code**  <br>*可选*|编码|integer (int32)|
|**data**  <br>*可选*|数据|integer (int64)|
|**message**  <br>*可选*|消息|string|


<a name="95cde29d19638e8b05882b3550fba87d"></a>
### User对象
用户表


|名称|说明|类型|
|---|---|---|
|**createDt**  <br>*可选*|创建时间|string (date-time)|
|**id**  <br>*可选*|主键|integer (int64)|
|**isDeleted**  <br>*可选*|是否删除 0正常 1 已删除|integer (int32)|
|**loginName**  <br>*可选*|登录名|string|
|**nickName**  <br>*可选*|昵称|string|
|**password**  <br>*可选*|密码|string|
|**updateDt**  <br>*可选*|更新时间|string (date-time)|


<a name="view"></a>
### View

|名称|类型|
|---|---|
|**contentType**  <br>*可选*|string|




<a name="securityscheme"></a>
## 安全

<a name="authorization"></a>
### Authorization
*类型* : apiKey  
*名称* : TOKEN  
*在* : HEADER



