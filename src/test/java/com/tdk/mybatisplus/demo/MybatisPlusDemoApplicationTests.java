package com.tdk.mybatisplus.demo;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.tdk.mybatisplus.demo.common.util.FreemarkerUtil;
import com.tdk.mybatisplus.demo.common.util.MybatisPlusBatchUpdateOrInsertUtil;
import com.tdk.mybatisplus.demo.common.util.TdkBeanUtils;
import com.tdk.mybatisplus.demo.sys.entity.User;
import freemarker.template.Template;
//import io.github.swagger2markup.GroupBy;
//import io.github.swagger2markup.Language;
//import io.github.swagger2markup.Swagger2MarkupConfig;
//import io.github.swagger2markup.Swagger2MarkupConverter;
//import io.github.swagger2markup.builder.Swagger2MarkupConfigBuilder;
//import io.github.swagger2markup.markup.builder.MarkupLanguage;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@Slf4j
@RunWith(SpringRunner.class)
class MybatisPlusDemoApplicationTests {

  @Autowired
  private JdbcTemplate                       jdbcTemplate;
  @Autowired
  private MybatisPlusBatchUpdateOrInsertUtil batchUpdateOrInsertUtil;

  @Test
  void test() throws IOException {
    //storeUserInfo2Db();
    //databaseDesignDocument("test");
    //createApiDoc("http://localhost:8080/v2/api-docs", "api");
  }

  public void storeUserInfo2Db() {
    User               user    =new User();
    Map<String,Object> userMap = Maps.newHashMap();
    userMap.put("loginName","tdk");
    userMap.put("nickName","tdk");
    userMap.put("password","");
    TdkBeanUtils.copyValue("测试对象属性拷贝",userMap,user);
    user.setCreateDt(LocalDateTime.now());
    user.setUpdateDt(LocalDateTime.now());
    batchUpdateOrInsertUtil.store2Db("测试增加用户", Lists.newArrayList(user),User.class);
  }
  /**
   * <p>
   * 数据库名称
   * </p>
   *
   *
   * @param databaseName  
   * @author: taodingkai
   * @since: 2021/9/17 10:20 
   */
  public void databaseDesignDocument(String databaseName) throws IOException {
    // 数据库设计说明文档word xml,可以用WPS等工具打开另存为.docx
    String         targetFileName  = "C:\\Users\\TDK\\Desktop\\数据库设计\\"+databaseName+"数据库设计说明文档三级标题.xml";
    BufferedWriter bufferedWriter  = new BufferedWriter(new FileWriter(targetFileName));
    String         targetFileName2 = "C:\\Users\\TDK\\Desktop\\数据库设计\\"+databaseName+"数据库设计说明文档四级标题.xml";
    BufferedWriter bufferedWriter2 = new BufferedWriter(new FileWriter(targetFileName2));
    try {
      // 查询数据库
      List<Map<String, Object>> tables = jdbcTemplate.queryForList(
          "SELECT "
              + "IFNULL(`TABLE_NAME`,\"\") AS `tableName`, "
              + "IFNULL(`TABLE_COMMENT`,\"\") AS `tableComment` \n"
              + "FROM `INFORMATION_SCHEMA`.`TABLES` \n"
              + "WHERE `TABLE_SCHEMA` = '" + databaseName
              + "'  AND `TABLE_NAME` NOT LIKE '%flyway%'"
              + "  ORDER BY `TABLE_NAME` ASC"
      );
      for (int i = 0; i < tables.size(); i++) {
        Map<String, Object> table     = tables.get(i);
        Object              tableName = table.get("tableName");
        List<Map<String, Object>> columns = jdbcTemplate.queryForList("SELECT \n"
            + "IFNULL(`ORDINAL_POSITION`,\"\") AS `ordinalPosition`, \n"
            + "IFNULL(`COLUMN_NAME`,\"\") AS `columnName`, \n"
            + "IFNULL(`COLUMN_COMMENT`,\"\") AS `columnComment`, \n"
            + "IFNULL(`COLUMN_TYPE`,\"\") AS `columnType`, \n"
            + "IFNULL(`COLUMN_DEFAULT`,\"\") AS `columnDefault`, \n"
            + "IF(`IS_NULLABLE` = 'YES', '否', '是' ) AS `isNullAble`, \n"
            + "IF( `COLUMN_KEY` = 'PRI', '是', '' ) AS `columnKey`\n"
            + "FROM `INFORMATION_SCHEMA`.`COLUMNS` \n"
            + "WHERE `TABLE_SCHEMA` = '" + databaseName
            + "'  AND `TABLE_NAME` ='" + tableName + "'");
        table.put("columns", columns);
      }
      /*for (int i=0;i<tables.size();i++) {
				Map<String, Object> table = tables.get(i);
				String tableString = "\n\n\n" + table.get("tableComment") + "(" + table.get("tableName") + ")\n"
						+ "中文名称\t\t\t" + table.get("tableComment") + "\n"
						+ "英文名\t\t\t" + table.get("tableName") + "\n"
						+ "描 述\t\t\t" + table.get("tableComment") + "\n"
						+ "结构信息\n"
						+ "序号\t字段名\t注释\t字段类型\t默认值\t是否为空\t是否为主键\n";
				bufferedWriter.write(tableString);
				bufferedWriter.flush();
				List<Map<String, Object>> columns = (List<Map<String, Object>>)table.get("columns");
				for (int j=0;j<columns.size();j++) {
					Map<String, Object> column = columns.get(j);
					String columnString = column.get("ordinalPosition") + "\t" + column.get("columnName") + "\t" + column.get("columnComment") + "\t"
							+ column.get("columnType") + "\t" + column.get("columnDefault") + "\t" + column.get("isNullAble") + "\t" + column.get("columnKey") + "\n";
					bufferedWriter.write(columnString);
					bufferedWriter.flush();
				}
			}*/
      Map<String, Object> data = Maps.newHashMap();
      data.put("tables", tables);
      Template template = FreemarkerUtil.getTemplate("数据库设计说明模板三级标题.ftl");
      template.process(data, bufferedWriter);
      Template template2 = FreemarkerUtil.getTemplate("数据库设计说明模板四级标题.ftl");
      template2.process(data, bufferedWriter2);
      tables.forEach(table->{
        System.out.println(table.get("tableName")+"\t"+table.get("tableComment"));
      });
    } catch (Exception e) {
      log.error("数据库设计说明文档生成失败", e);
    } finally {
      IOUtils.closeQuietly(bufferedWriter);
      IOUtils.closeQuietly(bufferedWriter2);
    }
  }

//  public static void createApiDoc(String apiDocUrl, String apiName) throws MalformedURLException {
//    Swagger2MarkupConfig config = new Swagger2MarkupConfigBuilder()
//        //.withMarkupLanguage(MarkupLanguage.ASCIIDOC)
//        .withMarkupLanguage(MarkupLanguage.MARKDOWN)
//        .withOutputLanguage(Language.ZH)
//        .withPathsGroupedBy(GroupBy.TAGS)
//        .build();
//
//    URL remoteSwaggerFile = new URL(apiDocUrl);
//
//    Path filePath = Paths.get("src/main/resources/"+ apiName);
//    Swagger2MarkupConverter converter = Swagger2MarkupConverter.from(remoteSwaggerFile)
//        .withConfig(config)
//        .build();
//
//    converter.toFile(filePath);
//    log.info("已生成MARKDOWN文件：{}",filePath.toAbsolutePath().toString());
//  }
}
