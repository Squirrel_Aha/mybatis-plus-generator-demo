package com.tdk.mybatisplus.demo.common.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;

/**
 * <p>
 * spring自己的定时任务配置
 * </p>
 *
 * @author: taodingkai
 * @modified:
 * @since: 2022/08/08 14:16
 */
@Configuration
public class TaskSchedulerConfig {
    @Bean
    public TaskScheduler taskScheduler() {
        ThreadPoolTaskScheduler taskScheduler = new ThreadPoolTaskScheduler();
        taskScheduler.setPoolSize(1000);
        taskScheduler.initialize();
        return taskScheduler;
    }
}