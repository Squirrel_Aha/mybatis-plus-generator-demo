package com.tdk.mybatisplus.demo.runner;

import com.tdk.mybatisplus.demo.api.HelloApi;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(value = 300)
@Slf4j
public class DefaultApplicationRunner implements ApplicationRunner {

  @Autowired
  private HelloApi helloApi;

  @Override
  public void run(ApplicationArguments applicationArguments) throws Exception {
    try {
      log.info(helloApi.hello("hello"));
    }catch (Exception e){
      log.error("helloApi.hello(\"hello\")调用失败",e);
    }
    
  }
}