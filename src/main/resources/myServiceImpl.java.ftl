package ${package.ServiceImpl};

import ${package.Entity}.${entity};
import ${package.Mapper}.${table.mapperName};
import ${package.Service}.${table.serviceName};
import ${superServiceImplClassPackage};
import org.springframework.stereotype.Service;
import java.util.Collection;

/**
 * <p>
 * ${table.comment!} 服务实现类
 * </p>
 *
 * @author ${author}
 * @since ${date}
 */
@Service
<#if kotlin>
open class ${table.serviceImplName} : ${superServiceImplClass}<${table.mapperName}, ${entity}>(), ${table.serviceName} {

}
<#else>
public class ${table.serviceImplName} extends ${superServiceImplClass}<${table.mapperName}, ${entity}> implements ${table.serviceName} {

    @Override
    public void batchInsertOnDuplicateKeyUpdate(Collection<${entity}> list) {
        baseMapper.batchInsertOnDuplicateKeyUpdate(list);
    }

    @Override
    public void batchInsertOnDuplicateKeyIgnore(Collection<${entity}> list) {
        baseMapper.batchInsertOnDuplicateKeyIgnore(list);
    }

}
</#if>
