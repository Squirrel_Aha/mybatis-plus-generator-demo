package com.tdk.mybatisplus.demo.actable;

import com.gitee.sunchenbin.mybatis.actable.annotation.Column;
import com.gitee.sunchenbin.mybatis.actable.annotation.ColumnType;
import com.gitee.sunchenbin.mybatis.actable.annotation.IsKey;
import com.gitee.sunchenbin.mybatis.actable.annotation.Table;
import com.gitee.sunchenbin.mybatis.actable.constants.MySqlTypeConstant;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 系统管理-权限
 * </p>
 *
 * @author: taodingkai
 * @modified:
 * @since: 2021/7/26 17:00
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "Role对象", description = "权限")
@Table(value = "Role",isSimple = true,comment = "权限")
public class Role {
  @IsKey
  @Column(comment = "权限id")
  private Long id;
  @Column(comment = "权限名称")
  @ColumnType(MySqlTypeConstant.VARCHAR)
  private String name;
  @Column(comment = "权限点集合")
  private String permissionPointCollection;
}