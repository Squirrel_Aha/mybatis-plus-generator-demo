package com.tdk.mybatisplus.demo.common.util;

import freemarker.template.Configuration;
import freemarker.template.Template;
import java.io.IOException;

/**
 * <p>
 * FreemarkerUtil
 * </p>
 *
 * @author: taodingkai
 * @modified:
 * @since: 2021/9/9 12:41
 */
public class FreemarkerUtil {
  private static Configuration CONFIGURATION = new Configuration(Configuration.VERSION_2_3_31);
  static {
    CONFIGURATION.setClassForTemplateLoading(FreemarkerUtil.class,"/");
  }
  public static Template getTemplate(String templateName) throws IOException {
    return CONFIGURATION.getTemplate(templateName);
  }
}
