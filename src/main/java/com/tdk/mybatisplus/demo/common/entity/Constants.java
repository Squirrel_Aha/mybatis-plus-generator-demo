package com.tdk.mybatisplus.demo.common.entity;

import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import com.google.common.eventbus.EventBus;
import org.springframework.scheduling.concurrent.CustomizableThreadFactory;

public class Constants {

  /**
   * 数字0
   */
  public static final Integer ZERO = 0;

  /**
   * 数字1
   */
  public static final Integer ONE = 1;
  
  /**
   * 数字2
   */
  public static final Integer TWO = 2;
  /**
   * 数字10
   */
  public static final Integer TEN = 10;

  /**
   * 数字1000
   */
  public static final Long A_THOUSAND = 1000L;
  
  /**
   * 字符串listObjs
   */
  public static final String LIST_OBJS = "listObjs";

  /**
   * 字符串saveBatch
   */
  public static final String SAVE_BATCH = "saveBatch";

  /**
   * 字符串updateBatchById
   */
  public static final String UPDATE_BATCH_BY_ID = "updateBatchById";
  /**
   * 字符串batchInsertOnDuplicateKeyUpdate
   */
  public static final String BATCH_INSERT_ON_DUPLICATE_KEY_UPDATE = "batchInsertOnDuplicateKeyUpdate";

  /**
   * 字符串batchInsertOnDuplicateKeyIgnore
   */
  public static final String BATCH_INSERT_ON_DUPLICATE_KEY_IGNORE = "batchInsertOnDuplicateKeyIgnore";

  /**
   * 时间格式 yyyy-MM-dd
   */
  public static final String DATE_PATTERN = "yyyy-MM-dd";
  /**
   * 时间格式 yyyy-MM-dd HH:mm:ss
   */
  public static final String DATE_TIME_PATTERN = "yyyy-MM-dd HH:mm:ss";
  /**
   * 时间格式 yyyy-MM-dd'T'HH:mm:ss.SSS
   */
  public static final String TIME_PATTERN_WITH_WITH_MILLI_SECOND ="yyyy-MM-dd'T'HH:mm:ss.SSS";
  /**
   * ISO时间格式 yyyy-MM-dd'T'HH:mm:ss.SSSZ
   */
  public static final String ISO_TIME_PATTERN_WITH_MILLI_SECOND="yyyy-MM-dd'T'HH:mm:ss.SSSZ";
  /**
   * 时间格式 yyyy-MM-dd HH:mm:ss.SSSSSS
   */
  public static final String TIME_PATTERN_WITH_NANO_SECOND = "yyyy-MM-dd HH:mm:ss.SSSSSS";
  /**
   * 时间格式 yyyy-MM-dd HH:mm:ss.SSSSSSZ
   */
  public static final String ISO_TIME_PATTERN_WITH_NANO_SECOND = "yyyy-MM-dd HH:mm:ss.SSSSSSZ";

  /**
   * 导入数据最大条数
   */
  public static final Integer IMPORT_MAX_ROW_SIZE = 3000;
  
  /**
   * 默认线程池
   */
  public static final ThreadPoolExecutor DEFAULT_THREAD_POOL = new ThreadPoolExecutor(
      Runtime.getRuntime().availableProcessors()-ONE,
      (Runtime.getRuntime().availableProcessors()-ONE) * TWO, 60L, TimeUnit.SECONDS,
      new SynchronousQueue<Runnable>(),new CustomizableThreadFactory("back-end-service"), new ThreadPoolExecutor.CallerRunsPolicy());

  /**
   * 默认的eventBus
   */
  public static final EventBus DEFAULT_EVENT_BUS = new EventBus("back-end-service");
}